// ==UserScript==
// @name         ncore list prettyfier
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://addons.mozilla.org/hu/firefox/addon/tampermonkey/
// @grant        none
// @include      https://ncore.cc/torrents.php*
// ==/UserScript==


// ========================= CLASS DEFINITIONS =========================
class Torrent {
    constructor(name, onClickUrl, imageUrl, imdb, type, uploadDate, size, downloaded, seed, leech) {
        this.name = name;
        this.onClickUrl = onClickUrl;
        this.imageUrl = imageUrl;
        this.imdb = imdb;
        this.type = type;
        this.size = size;
        this.uploadDate = uploadDate;
        this.downloaded = downloaded;
        this.seed = seed;
        this.leech = leech;
    }
}

// ========================= CONSTANT VARIABLES =========================
const NO_IMAGE_URL = "https://c-lj.gnst.jp/public/img/common/noimage.jpg?20180630050046";


// ========================= FUNCTION DEFINITIONS =========================

function removeElement(element) {
    element.parentNode.removeChild(element);
}

function removeAdds() {
    const banners = Array.from(document.querySelectorAll('#main_tartalom>center'))
    banners.forEach(bannerNode => removeElement(bannerNode));
}

// TODO inside
function removeListHeaders() {
    removeElement(document.querySelector('.lista_fej_t'));
    removeElement(document.querySelector('.lista_fej'));
    removeElement(document.querySelector('.lista_fej_b'));

    // TODO remove/reform this too when the new layout is ready
    // removeElement(document.querySelector('.box_alcimek_all'));
}

function removeInfoLine() {
    const infoLine = document.querySelector('.infocsik');
    removeElement(infoLine);
}

function removeFooters() {
    const footerInfo = document.querySelector('#hessteg');
    const footerSpace = document.querySelector('#div_body_space');
    const footer = document.querySelector('#footer_bg');
    removeElement(footerInfo);
    removeElement(footerSpace);
    removeElement(footer);
}

function removeHeader() {
    const headerLine = document.querySelector('#fej_csik_felso');
    const header = document.querySelector('#fej_fejlec_bg');
    removeElement(headerLine);
    removeElement(header);
}

function removeUnnecessaryContent() {
    removeAdds();
    removeListHeaders();
    removeInfoLine();
    removeFooters();
    removeHeader();
}

function getImageUrlFormElement(element) {
    if (!element) {
        return null;
    }
    const outerHTMLText = element.outerHTML;
    const startIndex = outerHTMLText.indexOf('https');
    const slicedOuterHTMLText = outerHTMLText.slice(startIndex);
    const url = slicedOuterHTMLText.split(',')[0].slice(0, -1);
    return url;
}

function getTorrentTypeFromElement(element) {
    // get type part from src
    const srcArray = element.src.split('/');
    const longType = srcArray[srcArray.length - 1];
    const longTypeArray = longType.split('_');
    longTypeArray.shift();
    // remove ".gif" ending
    const ending = longTypeArray[longTypeArray.length - 1];
    longTypeArray[longTypeArray.length - 1] = ending.slice(0, ending.indexOf('.'));
    // format raw type to string
    let torrentType = longTypeArray.length > 1 ? longTypeArray[0] + '_' + longTypeArray[1] : longTypeArray[0];
    torrentType = mapRawTorrentTypeToProperTorrentType(torrentType);
    return torrentType;
}

function mapRawTorrentTypeToProperTorrentType(rawType) {
    const options = Array.from(document.querySelector('#listazasi_tipus').children);
    let properType = options.find(option => option.value === rawType).innerText;
    return properType;
}

function getImdbRatingFromInfolinkElement(element) {
    if (!element) {
        return "N/A";
    }

    const rawText = element.innerText;
    const numberArray = rawText.match(/^\d+|\d+\b|\d+(?=\w)/g);
    let rating = '';
    numberArray.forEach((number, i) => {
        rating += i === 0 ? number + '.' : number;
    });
    return rating;
}

function getAllTorrentsPerPage() {
    const torrents = document.querySelectorAll('.box_torrent_all .box_torrent');
    const formattedTorrents = [];
    torrents.forEach(torrentBox => {
        const currentTorrent = new Torrent(
            torrentBox.querySelector('nobr').innerText, // name
            torrentBox.querySelector('nobr').parentElement.href, //onClickUrl
            getImageUrlFormElement(torrentBox.querySelector('.infobar .infobar_ico')), // imageUrl
            getImdbRatingFromInfolinkElement(torrentBox.querySelector('.infolink')),// ? torrentBox.querySelector('.infolink').innerText : null, // imdb
            getTorrentTypeFromElement(torrentBox.querySelector('.box_alap_img .categ_link')), // type
            torrentBox.querySelector('.box_feltoltve2').innerHTML.replace('<br>', ' '), // uploadDate
            torrentBox.querySelector('.box_meret2').innerText, // size
            torrentBox.querySelector('.box_d2').innerText, // downloaded
            torrentBox.querySelector('.box_s2').innerText, // seed
            torrentBox.querySelector('.box_l2').innerText // leech
        );
        formattedTorrents.push(currentTorrent);
    });
    return formattedTorrents;
}

function addMyStyle() {
    const head = document.querySelector('head');
    const myStyle = document.createElement('style');
    myStyle.innerHTML = `
        .container {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
        }

        .card-container {
            width: 150px;
            height: 200px;
            margin: 20px 10px;
        }

        .card-content {
            width: 150px;
            height: 200px;
            position: relative;
            transition: all .3s;
        }

        .card {
            width: 100%;
            height: 100%;
            box-shadow: 0px 0px 10px black;
            border-radius: 5px;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }

        .torrent-name {
            margin: 5px 10px 0px 10px;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }

        .card-layer {
            background-color: rgba(0, 0, 0, 0.7);
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            opacity: 0;
            transition: opacity .3s linear;
            display: flex;
            flex-direction: column;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
        }

        .card-layer:hover {
            opacity: 1;
        }

        .card-layer p {
            margin: 3px;
        }
        .card-layer p span{
            color: #E9E9EA;
        }

    `;
    head.appendChild(myStyle);
}

// TODO REMOVE IF NOT NEEDED
function addMyScript() {
    const body = document.querySelector('body');
    const myScript = document.createElement('script');
    myScript.innerHTML = ``;
    body.appendChild(myScript);
}

function createCard(torrentObj) {
    const card = `
      <a href="${torrentObj.onClickUrl}" target="_blank" class="card-container">
         <div class="card-content">

            <div class="card" style="background-image: url(${torrentObj.imageUrl || NO_IMAGE_URL});"></div>

            <div class="card-layer">
                 <h3>${torrentObj.type}</h3>
                 <p style="display: ${torrentObj.imdb === 'N/A' ? 'none' : 'block'}">IMDB: <span>${torrentObj.imdb}</span></p>
                 <p>Size: <span>${torrentObj.size}</span></p>
                 <p>Uploaded: <span>${getShortUploadedDate(torrentObj.uploadDate)}</span></p>
                 <p>Downloaded: <span>${torrentObj.downloaded}</span></p>
                 <p>Seed: <span>${torrentObj.seed}</span></p>
                 <p>Leech: <span>${torrentObj.leech}</span></p>
            </div>

         </div>
         <p class="torrent-name" title="${torrentObj.name}">${torrentObj.name}</p>
      </a>
    `;
    return card;
}

function removeOldTorrents() {
    const allTorrents = document.querySelector(".box_torrent_all");
    removeElement(allTorrents);
}

function addNewTorrentsLayout(torrents) {
    const container = document.createElement("DIV");
    container.classList.add("container");

    torrents.forEach(torrent => {
        const currentCard = createCard(torrent);
        container.innerHTML += currentCard;
    });

    const torrentListEnd = document.querySelector(".lista_lab");
    torrentListEnd.parentNode.insertBefore(container, torrentListEnd);
}

function getShortUploadedDate(longUploadedDate) {
    return longUploadedDate.substring(0, 10);
}

// ========================= CODE USAGE =========================
function init() {
    const torrents = getAllTorrentsPerPage();
    removeUnnecessaryContent();
    addMyStyle();

    // TODO: maybe later needed
    // addMyScript();
    removeOldTorrents();
    addNewTorrentsLayout(torrents);
}

function toggle() {
    if (localStorage.getItem('ncoreCards') === 'true') {
        localStorage.setItem('ncoreCards', 'false');
        location.reload();
    } else {
        localStorage.setItem('ncoreCards', 'true');
        init();
    }
}

(function () {
    'use strict';
    if (localStorage.getItem('ncoreCards') === 'true') {
        init();
    }

    document.addEventListener('keyup', (e) => {
        if (e.key === 'C' && e.shiftKey) {
            toggle();
        }
    });
})();
