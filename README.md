# ncore-tampermonkey-prettifier

*If you have an ncore account and want a prettier layout...*

## How to:
- Add `tempermonkey` to your browser
- Create a `new sricpt file` in it
- Copy the code **from** the `ncore-prettifier.js` **into** your `new script file`
- To switch beetween old and new layout use: `SHIFT + C` *(the new layout is turned off by default)*
- Enjoy it! **:)**


*Tested on the `nDefault` style*
